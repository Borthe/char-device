#include <linux/module.h>
#include <linux/kernel.h>

int init_module(void)
{   /* Constructor */
    printk(KERN_INFO "UNGS: Driver registrado\n");
    return 0;
}

void cleanup_module(void)
{
    /* Destructor */
    printk(KERN_INFO "UNGS: Driver desregistrado\n");    
}

/* MODULE_LICENCE("GPL"); No me deja crear el modulo si tiene esta linea */
MODULE_AUTHOR("MARTIN_BORNHOLDT");
MODULE_DESCRIPTION("Un segundo driver");