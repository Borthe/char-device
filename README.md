![UNGS](http://www.redbien.edu.ar/upload/img/Logo_UNGS_1.jpg "Universidad Nacional de General Sarmiento")

## Sistemas Operativos y Redes II

### Integrante

- Bornholdt Martin

### Profesores

- Alexander Agustin
- Gutierrez Pedro 

#### Segundo cuatrimestre, 2020

# Char Device

# Modulo de Kernel

Proyecto universitario perteneciente a la materia Sistemas Operativos y Redes II. 
- Implementación de 2 modulos.
    - Modulo simple.  
    - Modulo Char Device, implementa la encriptacion Cesar.

## Prerrequisitos

  - El programa funciona en SO Linux Ubuntu.

### Preparación

Instale Git para Ubuntu.

```sh
$ sudo apt-get install git
```

Descargue el proyecto.

```sh
$ sudo git pull https://gitlab.com/Borthe/char-device.git
```

### Instalación
Entre a la carpeta que desee probar del proyecto.

```sh
$ cd punto_1
```
o
```sh
$ cd punto_2
```

Ejecute el comando Make.

```sh
$ make
```

Instale el modulo, utilizando el archivo .ko.
```sh
$ insmod "modulo".ko
```

Para desinstalar el modulo.
```sh
$ rmmod "modulo"
```


### Ejemplo del Char Device.
Utilizando los comandos anteriores, podemos instalar el Char Device. De esta manera, podemos probar que realizandole un **echo** a la carpeta del modulo y luego un **cat**, el mensaje escrito va a aparecer encriptado. Para hacer esto [instalamos el modulo](#instalación), y configuramos de la siguiente manera.

Luego de instalar el modulo, utilizamos **dsmg**.
```sh
$ dmsg
```

![DMSG](https://gitlab.com/Borthe/char-device/-/raw/master/screens/p2/dmsg.png)

Creamos un fichero especial, con el comando en el mensaje del modulo.
```sh
$ mknod /dev/chardevice c "mayor" "menor"
```
![MKNOD](https://gitlab.com/Borthe/char-device/-/raw/master/screens/p2/mknod.png)

Otorgamos los permisos de escritura y lectura.
```sh
$ chmod 777 /dev/chardevice
```
![CHMOD](https://gitlab.com/Borthe/char-device/-/raw/master/screens/p2/chmod.png)

Hacemos un **echo** con el mensaje que queremos que el modulo lea y encripte. Luego lo vemos con **cat**.
```sh
$ echo "mensaje" > /dev/chardevice
```

```sh
$ cat /dev/chardevice
```
![ECHO-CAT](https://gitlab.com/Borthe/char-device/-/raw/master/screens/p2/echocat.png)

### ¿Como funciona?
El modulo escribe lo que se le agrege a su fichero especial y le aplica la **Encriptación Cesar**. Consiste en mover el caracter donde se este posicionado **n** posiciones hacia la derecha en el abecedario. Luego, cuando se quiera leer, el mensaje va a aparecer encriptado, sin poder recuperar el mensaje original, a menos que se conozca cuantas veces se corrío el caracter.
Este valor se encuentra constante en el código(en la suma "ch + 9", **9 es la n posiciones hacia la derecha**), pero puede ser ingresado para que varie.

![ECHO-CAT](https://gitlab.com/Borthe/char-device/-/raw/master/screens/p2/codigoCaesar.png)
