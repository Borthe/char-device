#include "chardev.h"

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

IOCTL_SET_MSG ( int file_desc, char *message )
{
    int ret_val;

    ret_val = ioctl ( file_desc, IOCTL_SET_MSG, message );

    if ( ret_val < 0 )
    {
        printf ( "IOCTL_SET_MSG fallo: %d\n", ret_val );
        exit ( -1 ); 
    }
}

IOCTL_GET_MSG ( int file_desc )
{
    int ret_val;
    char message [ 100 ];

    ret_val = ioctl ( file_desc, IOCTL_GET_MSG, message );

    if ( ret_val < 0)
    {
        printf ( "IOCTL_GET_MSG fallo: %d\n", ret_val );
        exit ( -1 );
    }

    printf ( "GET_MSG mensaje: %s\n", message );
}

IOCTL_GET_NTH_BYTE ( int file_desc )
{
    int i;
    char c;

    printf ( "GET_NTH_BYTE mensaje:");

    i = 0;

    do
    {
        c = ioctl ( file_desc, IOCTL_GET_NTH_BYTE, i++ );

        if ( c < 0 )
        {
            printf ( "IOCTL_GET_NTH_BYTE fallo en el byte %d\n", i );
            exit ( -1 );
        }

        putchar( c );    
    } while ( c != 0 );  
    
    putchar ( '\n' );
}

main ()
{
    int file_desc, ret_val;
    char *msg = "Mensaje enviado por IOCTL\n";

    file_desc = open ( DEVICE_FILE_NAME, 0 );
    
    if ( file_desc < 0 )
    {
        printf ( "No se pudo abrir el archivo del device: %s\n", DEVICE_FILE_NAME );
        exit ( -1 );
    }

    IOCTL_GET_NTH_BYTE ( file_desc );
    IOCTL_GET_MSG ( file_desc );
    IOCTL_SET_MSG ( file_desc, msg );

    close ( file_desc );
}
