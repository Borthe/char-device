#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

#include "chardev.h"

#define SUCCESS 0
#define DEVICE_NAME "chardevice"
#define BUF_LEN 80

static int Device_Open = 0; /* Para reemplazar el 0 del return */

static char msg[BUF_LEN];
static char *msg_Ptr;

/* Encriptado Cesar */
char cesar_encript( char ch )
{
    if ( ch >= 65 && ch <= 90 )
    {
        return ch = ( ch + 9 - 65 )%26 + 65;
    }
    else
    {
        return ch = ( ch + 9 - 97 )%26 + 97;
    } 
    
    return 0;
}

/* Apertura del modulo */
static int c_device_open ( struct inode *inode, struct file *filp )
{
    #ifdef DEBUG
        printk ( KERN_INFO "Char Device abierto( %p )\n", filp );
    #endif

    if ( Device_Open )
    {
        return -EBUSY;
    } 

    Device_Open++;

    msg_Ptr = msg;
    try_module_get ( THIS_MODULE );
    return SUCCESS;
}

/* Cierre del modulo */
int c_device_release ( struct inode *inode, struct file *filp )
{
    #ifdef DEBUG
        printk ( KERN_INFO "Char Device liberado( %p, %p )\n", inode, filp );
    #endif

    Device_Open--;

    module_put ( THIS_MODULE );
    return SUCCESS;
}

/* Lectura del modulo */
ssize_t c_device_read ( struct file *filp, char *buf, size_t count, loff_t * f_pos )
{
    int bytes_read = 0;

    #ifdef DEBUG
        printk ( KERN_INFO "Char Device leyendo( %p, %p, %d )\n", filp, buf, count );
    #endif

    if ( *msg_Ptr == 0 )
    {
        return 0;
    }

    while ( count && *msg_Ptr )
    {
        put_user ( *( msg_Ptr++ ), buf++ );
        count--;
        bytes_read++;
    }

    #ifdef DEBUG
        printk ( KERN_INFO "Leidos %d bytes, %d quedan\n", bytes_read, count );
    #endif

    return bytes_read;
}

/* Escritura del modulo */
ssize_t c_device_write ( struct file *filp, const char *buf, size_t count, loff_t *f_pos )
{
    #ifdef DEBUG
        printk ( KERN_INFO "Char Device escribiendo( %p, %s, %d )\n", filp, buf, count );
    #endif

    int i;
    for ( i = 0; i < count && i < BUF_LEN; i++ )
    {
        get_user ( msg[i], buf + i );
        msg[i] = cesar_encript ( msg[i] );
    }

    msg_Ptr = msg;
    return i;
}

long c_device_ioctl ( struct file *filp, unsigned int ioctl_num, unsigned long ioctl_param )
{
    int i;
    char *temp;
    char ch;

    switch ( ioctl_num )
    {
    case IOCTL_SET_MSG:
        temp = ( char * )ioctl_param;
        get_user ( ch, temp );

        for ( i = 0; ch && i < BUF_LEN; i++, temp ++)
        {
            get_user( ch, temp );
        }

        c_device_write ( filp, ( char * )ioctl_param, i ,0 );
        
        break;
    
    case IOCTL_GET_MSG:
        i = c_device_read ( filp, ( char * )ioctl_param, BUF_LEN ,0 );

        put_user ( '\0', ( char * )ioctl_param + i );
        
        break;
    
    case IOCTL_GET_NTH_BYTE:
        return msg [ ioctl_param ];

        break;
    }

    return SUCCESS;
}

/* Declaramos el struct para el acceso */
struct file_operations fops = 
{
    .read = c_device_read,
    .write = c_device_write,
    .unlocked_ioctl = c_device_ioctl,
    .open = c_device_open,
    .release = c_device_release,
};

/* Constructor */
int init_module ( void )
{
    int ret_val;

    ret_val = register_chrdev ( MAJOR_NUM, DEVICE_NAME, &fops ); /* Estrictamente en el 10, porque el 0 lo hace dinamico. */

    if ( ret_val < 0 )
    {
        printk ( KERN_INFO "Fallo conexion, no encontro el major number.\n", MAJOR_NUM );
        return ret_val;
    }

    printk ( KERN_INFO "%s\n El major number es %d.\n", "Char Device: Driver creado.", MAJOR_NUM );
    printk ( KERN_INFO "Se recomienda usar 'mknod /dev/%s c %d 0'\n", DEVICE_NAME, MAJOR_NUM );

    return SUCCESS;
}

/* Destructor */
void cleanup_module ( void )
{
    unregister_chrdev ( MAJOR_NUM, DEVICE_NAME );
    #if 0
        int ret;

        ret = unregister_chrdev ( MAJOR_NUM, DEVICE_NAME );

        if ( ret < 0 )
        {
            printk ( KERN_ALERT "Error al evaporizar el device\n");
        }    
    #endif

    printk ( KERN_INFO "Char Device: Driver evaporizado\n" );
}

MODULE_AUTHOR("MARTIN_BORNHOLDT");
MODULE_DESCRIPTION("Un segundo driver");
